/*!
 * This is the core communication demo
 * more to be added
 */

#include <stdio.h>
#include <stdlib.h>
#include <stropts.h>
#include <string.h>
#include <unistd.h>

#define BUFMAX 100

int tick = 0;

/*!
 * Perform one simulation step
 */
void doStep()
{
	char message[BUFMAX];
	if(fgets(message,BUFMAX,stdin) != NULL)
	{
		memset(message,0,BUFMAX);
		sprintf(message,"Step %d",tick);
		fwrite(message,sizeof(char),strlen(message),stdout);
		fflush(stdout);
		tick++;
	}
}

/*!
 * Perform one simulation step
 */
void init()
{
	char message[BUFMAX];
	fgets(message,BUFMAX,stdin);
	memset(message,0,BUFMAX);
	sprintf(message,"Bot 0.1");
	fwrite(message,sizeof(char),strlen(message),stdout);
	fflush(stdout);
}

/*!
 *  Send a exit signal to indicate that i will end, then give a short time to
 *  allow simulator time to handle my data and eventually to kill me.
 */
void end()
{
	char message[BUFMAX];
	sprintf(message,"exit");
	fwrite(message,sizeof(char),strlen(message),stdout);
	sleep(0.1);
}

/*!
 *  Run simulation, step by step, make an clean exit at the end.
 */
int main()
{
	init();
	for(int i=0; i<10; i++)
	{
		doStep();
	}
	end();
}
