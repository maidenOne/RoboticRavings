#!/usr/bin/env python

import pipe
import sys
import time

if __name__ == "__main__":
    # Execute bot
    p = pipe.Pipe("./"+sys.argv[1], timeout = 1)
    print 'Writing Server Version'
    p.write("1234\n");
    # Try to read something. At this stage no data should be ready to be read
    print 'Getting Botname %s' % p.readlines()
    tick = 0

    while(1):
      # If the execution did not hang, the following line is executed
      p.write("Hello\n")
      # Now some data should be available
      data = p.readlines()
      print  tick, 'Got:', data
      tick+=1
      if 'exit' in data:
        break
    p.close()
